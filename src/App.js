import NavMenu from './components/NavMenu';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import { Container } from 'react-bootstrap';
import Article from './pages/Article';
import Author from './pages/Author';
import Category from './pages/Category';
import './style/style.css';
import ArticleDetail from './components/ArticleDetail';

import { langeContext } from './context/langContext';
import { useEffect, useState } from 'react';
import { strings } from './localization/localization';

function App() {
	const [lang, setLang] = useState();

	useEffect(() => {
		if (localStorage.getItem('lang')) {
			strings.setLanguage(localStorage.getItem('lang'));
			setLang(localStorage.getItem('lang'));
		}
	}, []);

	return (
		<langeContext.Provider value={{ lang, setLang }}>
			<Router>
				<NavMenu />
				<Container>
					<Switch>
						<Route exact path='/' component={Home} />
						<Route exact path='/article-update/:id' component={Article} />
						<Route exact path='/article/:id' component={ArticleDetail} />
						<Route exact path='/article' component={Article} />
						<Route path='/author' component={Author} />
						<Route path='/category' component={Category} />
					</Switch>
				</Container>
			</Router>
		</langeContext.Provider>
	);
}

export default App;
