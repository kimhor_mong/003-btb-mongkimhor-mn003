import React from 'react';
import { useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { onClearArticle, onFetchArticleById } from '../redux/actions/articleAction';

function ArticleDetail() {
	const { id } = useParams();
	const article = useSelector((state) => state.articles.article);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchArticleById(id));
		return () => {
			dispatch(onClearArticle());
		};
	}, []);
	return (
		article && (
			<div className='mt-3'>
				<Row>
					<h2>{article.title}</h2>
					<Col md={6}>
						<img src={article.image} className='w-100' alt='' style={{ objectFit: 'fill' }} />
						<p>{article.description}</p>
					</Col>
					<Col md={6}>
						<h4>Comment</h4>
					</Col>
				</Row>
			</div>
		)
	);
}

export default ArticleDetail;
