import React, { useContext } from 'react';
import { useState } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button, Container } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { langeContext } from '../context/langContext';
import { strings } from '../localization/localization';

function NavMenu() {
	const [language, setLanguage] = useState([
		{
			title: 'English',
			key: 'en',
		},
		{
			title: 'Khmer',
			key: 'kh',
		},
	]);

	const context = useContext(langeContext);

	const changeLang = (lang) => {
		strings.setLanguage(lang);
		context.setLang(lang);
		localStorage.setItem('lang', lang);
	};

	return (
		<Navbar bg='primary' variant='dark' expand='md'>
			<Container>
				<Navbar.Brand as={Link} to='/'>
					AMS Redux
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='navbarScroll' />
				<Navbar.Collapse id='navbarScroll'>
					<Nav className='me-auto my-2 my-lg-0' navbarScroll>
						<Nav.Link as={Link} to='/'>
							{strings.home}
						</Nav.Link>
						<Nav.Link as={NavLink} to='/article'>
							{strings.article}
						</Nav.Link>
						<Nav.Link as={NavLink} to='/author'>
							{strings.author}
						</Nav.Link>
						<Nav.Link as={NavLink} to='/category'>
							{strings.category}
						</Nav.Link>

						<NavDropdown title={strings.language} id='navbarScrollingDropdown'>
							{language.map((lang, index) => (
								<NavDropdown.Item key={index} onClick={() => changeLang(lang.key)}>
									{lang.title}
								</NavDropdown.Item>
							))}
						</NavDropdown>
					</Nav>
					<Form className='d-flex'>
						<FormControl type='search' placeholder={strings.search} className='me-2' aria-label='Search' />
						<Button variant='light'>{strings.search}</Button>
					</Form>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default NavMenu;
