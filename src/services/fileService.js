import api from '../utils/api';

export const uploadImage = async (file) => {
	let formData = new FormData();
	formData.append('image', file);

	let response = await api.post('images', formData);
	return response.data.url;
};
