import api from '../utils/api';

export const fetchCategories = async () => {
	let response = await api.get('/category');
	return response.data.data;
};

export const postCategory = async (newCategory) => {
	let response = await api.post('/category', newCategory);
	return response.data;
};

export const deleteCategoryById = async (categoryId) => {
	let response = await api.delete('/category/' + categoryId);
	return response.data.message;
};

export const updateCategoryById = async (categoryId, updateCategory) => {
	let response = await api.put('/category/' + categoryId, updateCategory);
	return response.data;
};
