import api from '../utils/api';

export const fetchAuthors = async () => {
	let response = await api.get('/author');
	return response.data.data;
};

export const postAuthor = async (newAuthor) => {
	let response = await api.post('/author', newAuthor);
	return response.data;
};

export const deleteAuthorById = async (authorId) => {
	let response = await api.delete('/author/' + authorId);
	return response.data.message;
};

export const updateAuthorById = async (authorId, updatedAuthor) => {
	let response = await api.put('/author/' + authorId, updatedAuthor);
	return response.data;
};
