import api from '../utils/api';

export const fetchArticles = async (page) => {
	let response = await api.get(`/articles?page=${page}&size=8`);
	return response.data;
};

export const fetchArticleById = async (id) => {
	let response = await api.get('/articles/' + id);
	return response.data.data;
};

export const postArticle = async (newArticle) => {
	let response = await api.post('/articles', newArticle);
	return response.data;
};

export const deleteArticleById = async (articleId) => {
	await api.delete('/articles/' + articleId);
};

export const updateArticleById = async (articleId, updatedArticle) => {
	let response = await api.patch('/articles/' + articleId, updatedArticle);
	return response.data;
};
