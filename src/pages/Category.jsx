import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { strings } from '../localization/localization';

import {
	onDeleteCategoryById,
	onFetchCategories,
	onPostCategory,
	onUpdateCategoryById,
} from '../redux/actions/categoryAction';

function Author() {
	const [selectedId, setSelectedId] = useState('');
	const [category, setCategory] = useState('');

	const categories = useSelector((state) => state.categories.categories);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchCategories());
	}, []);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();

		let newCategory = {
			name: category,
		};

		if (selectedId) {
			// update category
			dispatch(onUpdateCategoryById(selectedId, newCategory))
				.then((message) => {
					alert(message);
					resetForm();
				})
				.catch((error) => alert(error.message));
		} else {
			// insert new author
			dispatch(onPostCategory(newCategory))
				.then((message) => {
					alert(message);
					resetForm();
				})
				.catch((error) => alert(error.message));
		}
	};

	const onDelete = (id) => {
		dispatch(onDeleteCategoryById(id))
			.then((message) => alert(message))
			.catch((error) => alert(error.message));

		// if selected category was deleted, reset form
		if (id === selectedId) resetForm();
	};

	let resetForm = () => {
		setSelectedId('');
		setCategory('');
	};

	return (
		<Container>
			<h1 className='my-3'>{strings.category}</h1>
			<Row>
				<Col md={11}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Control
								type='text'
								placeholder={strings.inputCategory}
								value={category}
								onChange={(e) => setCategory(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>
					</Form>
				</Col>
				<Col md={1}>
					<Button variant={selectedId ? 'warning' : 'dark'} onClick={onAddOrUpdate}>
						{selectedId ? strings.update : strings.save}
					</Button>
				</Col>
			</Row>

			<Table striped bordered hover className='mt-2'>
				<thead>
					<tr>
						<th>{strings.No}</th>
						<th>{strings.categoryName}</th>
						<th>{strings.action}</th>
					</tr>
				</thead>
				<tbody>
					{categories.map((category, index) => (
						<tr key={category._id}>
							<td>{index + 1}</td>
							<td>{category.name}</td>

							<td>
								<Button
									size='sm'
									variant='warning'
									onClick={() => {
										setSelectedId(category._id);
										setCategory(category.name);
									}}
								>
									{strings.edit}
								</Button>{' '}
								<Button size='sm' variant='danger' onClick={() => onDelete(category._id)}>
									{strings.delete}
								</Button>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
}

export default Author;
