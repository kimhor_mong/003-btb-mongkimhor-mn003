import React, { useState, useEffect } from 'react';
import { Col, Row, Form, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { strings } from '../localization/localization';
import { onClearArticle, onPostArticle, onUpdateArticleById } from '../redux/actions/articleAction';
import { onFetchAuthors } from '../redux/actions/authorAction';
import { fetchArticleById } from '../services/articleService';
import { uploadImage } from '../services/fileService';

function Article() {
	const { id } = useParams();

	const [title, setTitle] = useState('');
	const [authorId, setAuthorId] = useState('');
	const [description, setDescription] = useState('');
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);

	const authors = useSelector((state) => state.authors.authors);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchAuthors());
		setFormData();
		return () => {
			dispatch(onClearArticle());
		};
	}, []);

	const setFormData = () => {
		if (id) {
			fetchArticleById(id).then((article) => {
				setTitle(article.title);
				if (article.author) setAuthorId(article.author._id);
				setDescription(article.description);
				setImageURL(article.image);
			});
		}
	};

	const onAddOrUpdate = async (e) => {
		e.preventDefault();
		let article = {
			title,
			author: authorId,
			description,
			image: imageURL,
		};

		if (imageFile) {
			let url = await uploadImage(imageFile);
			article.image = url;
		}

		if (id) {
			dispatch(onUpdateArticleById(id, article))
				.then((message) => alert(message))
				.catch((error) => console.log(error.message));
		} else {
			dispatch(onPostArticle(article))
				.then((message) => alert(message))
				.catch((error) => console.log(error.message));
		}
	};

	return (
		<div>
			<Row className='my-3'>
				<h1>{id ? strings.updateArticle : strings.addArticle}</h1>

				<Col md={8}>
					<Form>
						<Form.Group controlId='title' className='my-2'>
							<Form.Label>{strings.title}</Form.Label>
							<Form.Control
								type='text'
								placeholder={strings.title}
								value={title}
								onChange={(e) => setTitle(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Form.Group controlId='description' className='my-2'>
							<Form.Label>{strings.author}</Form.Label>
							<Form.Control
								as='select'
								aria-label='Choosse Category'
								onChange={(e) => setAuthorId(e.target.value)}
							>
								<option disabled selected>
									{strings.selectAuthor}
								</option>
								{authors.map((author) => (
									<option key={author._id} value={author._id} selected={authorId === author._id}>
										{author.name}
									</option>
								))}
							</Form.Control>
						</Form.Group>

						<Form.Group controlId='description' className='my-2'>
							<Form.Label>{strings.description}</Form.Label>
							<Form.Control
								as='textarea'
								rows={4}
								placeholder={strings.description}
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>
					</Form>
				</Col>
				<Col md={4} className='mt-2'>
					<img className='w-100' height='200px' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>
			<Button variant='primary' type='submit' onClick={onAddOrUpdate}>
				{id ? strings.update : strings.submit}
			</Button>
		</div>
	);
}

export default Article;
