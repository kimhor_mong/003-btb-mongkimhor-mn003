import React, { useEffect, useState } from 'react';
import { Table, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { strings } from '../localization/localization';
import { onDeleteAuthorById, onFetchAuthors, onPostAuthor, onUpdateAuthorById } from '../redux/actions/authorAction';
import { uploadImage } from '../services/fileService';

function Author() {
	const [selectedId, setSelectedId] = useState('');
	const [authorName, setAuthorName] = useState('');
	const [email, setEmail] = useState('');
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);

	const { authors } = useSelector((state) => state.authors);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchAuthors());
	}, [authors]);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();

		let newAuthor = {
			name: authorName,
			email,
		};

		if (imageFile) {
			let url = await uploadImage(imageFile);
			newAuthor.image = url;
		} else {
			newAuthor.image = imageURL;
		}

		if (selectedId) {
			// update author
			dispatch(onUpdateAuthorById(selectedId, newAuthor))
				.then((message) => {
					alert(message);
					resetForm();
				})
				.catch((error) => alert(error.message));
		} else {
			// insert new author
			dispatch(onPostAuthor(newAuthor))
				.then((message) => {
					alert(message);
					resetForm();
				})
				.catch((error) => alert(error.message));
		}
	};

	const onDelete = (id) => {
		// if selected author was deleted, reset form
		if (id === selectedId) resetForm();

		dispatch(onDeleteAuthorById(id))
			.then((message) => alert(message))
			.catch((error) => alert(error.message));
	};

	let resetForm = () => {
		setAuthorName('');
		setEmail('');
		setImageURL('https://designshack.net/wp-content/uploads/placeholder-image.png');
		setImageFile(null);
		setSelectedId('');
	};

	return (
		<div className='my-3'>
			<h1 className='my-3'>{strings.author}</h1>
			<Row>
				<Col md={8}>
					<Form>
						<Form.Group controlId='title' className='my-2'>
							<Form.Label>{strings.authorName}</Form.Label>
							<Form.Control
								type='text'
								placeholder={strings.authorName}
								value={authorName}
								onChange={(e) => setAuthorName(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Form.Group controlId='email' className='my-2'>
							<Form.Label>{strings.email}</Form.Label>
							<Form.Control
								type='text'
								placeholder={strings.email}
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>
					</Form>
				</Col>
				<Col md={4}>
					<img className='w-100' height='200px' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
				<Col md={1} className='mb-4'>
					<Button variant={selectedId ? 'warning' : 'primary'} onClick={onAddOrUpdate}>
						{selectedId ? strings.update : strings.submit}
					</Button>
				</Col>
			</Row>

			<Table striped bordered hover>
				<thead>
					<tr>
						<th>{strings.No}</th>
						<th>{strings.authorName}</th>
						<th>{strings.email}</th>
						<th>{strings.image}</th>
						<th>{strings.action}</th>
					</tr>
				</thead>
				<tbody>
					{authors.map((author, index) => (
						<tr key={author._id}>
							<td>{index + 1}</td>
							<td>{author.name}</td>
							<td>{author.email}</td>
							<td>
								<img style={{ objectFit: 'cover', height: '80px' }} src={author.image} alt='' />
							</td>
							<td>
								<Button
									size='sm'
									variant='warning'
									onClick={() => {
										setSelectedId(author._id);
										setAuthorName(author.name);
										setEmail(author.email);
										setImageURL(author.image);
									}}
								>
									{strings.edit}
								</Button>{' '}
								<Button size='sm' variant='danger' onClick={() => onDelete(author._id)}>
									{strings.delete}
								</Button>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</div>
	);
}

export default Author;
