import React, { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchArticles, onDeleteArticleById } from '../redux/actions/articleAction';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

import { strings } from '../localization/localization';

function Home() {
	const { articles, page, limit, totalPage } = useSelector((state) => state.articles);
	const dispatch = useDispatch();

	const history = useHistory();
	useEffect(() => {
		dispatch(onFetchArticles(page));
	}, []);

	const onPageChange = ({ selected }) => {
		dispatch(onFetchArticles(selected + 1));
	};

	return (
		<div>
			<Row className='mt-3'>
				<Col md={3} className='mb-3'>
					<Card>
						<Card.Img
							variant='top'
							src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTtQZutuVAXi7boyDVdqMC_4xGIHBVaIUT7X8_bCZa6AYItzKGimXQg7rUXoEx-hLCnsaw&usqp=CAU'
							style={{ objectFit: 'contain', height: '180px' }}
						/>
						<Card.Body>
							<Card.Title>Please Login</Card.Title>
							<Button variant='primary' size='sm'>
								Login with Google
							</Button>
						</Card.Body>
					</Card>
				</Col>
				<Col md={9}>
					<h2 className='mb-4'>{strings.category}</h2>
					<Row>
						{articles.map((article) => (
							<Col md={3} className='mb-3' key={article._id}>
								<Card>
									<Card.Img
										variant='top'
										style={{ objectFit: 'cover', height: '150px' }}
										src={article.image}
									/>
									<Card.Body>
										<Card.Title>{article.title}</Card.Title>
										<Card.Text className='text-line-3'>{article.description}</Card.Text>
										<Button
											className='my-1'
											variant='primary'
											size='sm'
											onClick={() => {
												history.push(`/article/${article._id}`);
											}}
										>
											Read
										</Button>

										<Button
											className='my-1'
											variant='warning'
											size='sm'
											className='mx-1'
											onClick={() => {
												history.push(`/article-update/${article._id}`);
											}}
										>
											Edit
										</Button>
										<Button
											className='my-1'
											variant='danger'
											size='sm'
											onClick={() => {
												dispatch(onDeleteArticleById(article._id));
											}}
										>
											Delete
										</Button>
									</Card.Body>
								</Card>
							</Col>
						))}
					</Row>

					<Row>
						<Col className='d-flex justify-content-center my-3'>
							<ReactPaginate
								pageCount={totalPage}
								onPageChange={onPageChange}
								containerClassName='pagination'
								activeClassName='active'
								pageClassName='page-item'
								pageLinkClassName='page-link'
								previousClassName='page-item'
								nextClassName='page-item'
								nextLinkClassName='page-link'
								previousLinkClassName='page-link'
								nextLabel={strings.next}
								previousLabel={strings.previous}
							/>
						</Col>
					</Row>
				</Col>
			</Row>
		</div>
	);
}

export default Home;
