import { authorActionType } from './actionTypes/authorActionType';
import { deleteAuthorById, fetchAuthors, postAuthor, updateAuthorById } from '../../services/authorService';

export const onFetchAuthors = () => async (dispatch) => {
	const authors = await fetchAuthors();

	dispatch({
		type: authorActionType.FETCH_AUTHORS,
		payload: authors,
	});
};

export const onPostAuthor = (newAuthor) => async (dispatch) => {
	const response = await postAuthor(newAuthor);

	dispatch({
		type: authorActionType.ADD_AUTHOR,
		payload: response.data,
	});

	return Promise.resolve(response.message);
};

export const onDeleteAuthorById = (id) => async (dispatch) => {
	const message = await deleteAuthorById(id);
	dispatch({ type: authorActionType.DELETE_AUTHOR, payload: id });
	return Promise.resolve(message);
};

export const onUpdateAuthorById = (authorId, updatedAuthor) => async (dispatch) => {
	const response = await updateAuthorById(authorId, updatedAuthor);

	const author = {
		...response.data,
		name: updatedAuthor.name,
		email: updatedAuthor.email,
		image: updatedAuthor.image,
	};

	dispatch({
		type: authorActionType.UPDATE_AUTHOR,
		payload: author,
	});

	return Promise.resolve(response.message);
};
