import {
	fetchArticles,
	fetchArticleById,
	postArticle,
	deleteArticleById,
	updateArticleById,
} from '../../services/articleService';
import { articleActionType } from './actionTypes/articleActionType';

export const onFetchArticles = (page) => async (dispatch) => {
	const reponse = await fetchArticles(page);
	dispatch({
		type: articleActionType.FETCH_ARTICLES,
		payload: reponse,
	});
};

export const onFetchArticleById = (id) => async (dispatch) => {
	const article = await fetchArticleById(id);

	dispatch({
		type: articleActionType.FETCH_ARTICLE,
		payload: article,
	});
};

export const onPostArticle = (newArticle) => async (dispatch) => {
	const response = await postArticle(newArticle);

	dispatch({
		type: articleActionType.ADD_ARTICLE,
		payload: response.data,
	});
	return Promise.resolve(response.message);
};

export const onDeleteArticleById = (articleId) => async (dispatch) => {
	await deleteArticleById(articleId);
	dispatch({
		type: articleActionType.DELETE_ARTICLE,
		payload: articleId,
	});
};

export const onClearArticle = () => {
	return {
		type: articleActionType.CLEAR_ARTICLE,
	};
};

export const onUpdateArticleById = (articleId, updatedArticle) => async (dispatch) => {
	const response = await updateArticleById(articleId, updatedArticle);
	dispatch({
		type: articleActionType.UPDATE_ARTICLE,
		payload: response.data,
	});

	return Promise.resolve(response.message);
};
