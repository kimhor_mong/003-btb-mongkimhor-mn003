import { deleteCategoryById, fetchCategories, postCategory, updateCategoryById } from '../../services/categoryService';
import { categoryActionType } from './actionTypes/categoryActionType';

export const onFetchCategories = () => async (dispatch) => {
	const categories = await fetchCategories();
	dispatch({
		type: categoryActionType.FETCH_CATEGORIES,
		payload: categories,
	});
};

export const onPostCategory = (newCategory) => async (dispatch) => {
	const response = await postCategory(newCategory);

	dispatch({
		type: categoryActionType.ADD_CATEGORY,
		payload: response.data,
	});

	return Promise.resolve(response.message);
};

export const onUpdateCategoryById = (categoryId, updatedCategory) => async (dispatch) => {
	const response = await updateCategoryById(categoryId, updatedCategory);

	const category = { ...response.data, name: updatedCategory.name };

	dispatch({
		type: categoryActionType.UPDATE_CATEGORY,
		payload: category,
	});

	return Promise.resolve(response.message);
};

export const onDeleteCategoryById = (categoryId) => async (dispatch) => {
	const message = await deleteCategoryById(categoryId);
	dispatch({
		type: categoryActionType.DELETE_CATEGORY,
		payload: categoryId,
	});

	return Promise.resolve(message);
};
