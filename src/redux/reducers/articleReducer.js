import { articleActionType } from '../actions/actionTypes/articleActionType';

const initialState = {
	articles: [],
	article: null,
	page: 1,
	limit: 0,
	totalPage: 0,
};

const articleReducer = (state = initialState, action) => {
	switch (action.type) {
		case articleActionType.CLEAR_ARTICLE:
			return { ...state, article: null };
		case articleActionType.FETCH_ARTICLES:
			return {
				...state,
				articles: [...action.payload.data],
				page: action.payload.page,
				limit: action.payload.limit,
				totalPage: action.payload.total_page,
			};
		case articleActionType.FETCH_ARTICLE:
			return { ...state, article: action.payload };
		case articleActionType.ADD_ARTICLE:
			return { ...state, articles: [action.payload, ...state.articles] };
		case articleActionType.DELETE_ARTICLE:
			return { ...state, articles: state.articles.filter((article) => article._id !== action.payload) };
		case articleActionType.UPDATE_ARTICLE:
			return {
				...state,
				articles: state.articles.map((article) => {
					if (article._id === action.payload._id) {
						article.title = action.payload.title;
						article.author = action.payload.author;
						article.description = action.payload.description;
					}
					return article;
				}),
			};

		default:
			return state;
	}
};

export default articleReducer;
