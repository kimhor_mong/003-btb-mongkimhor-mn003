import { authorActionType } from '../actions/actionTypes/authorActionType';

const initialState = {
	authors: [],
	currentAuthors: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case authorActionType.FETCH_AUTHORS:
			return {
				...state,
				authors: [...action.payload],
			};

		case authorActionType.ADD_AUTHOR:
			return { ...state, authors: [action.payload, ...state.authors] };
		case authorActionType.DELETE_AUTHOR:
			return { ...state, authors: state.authors.filter((author) => author._id !== action.payload) };
		case authorActionType.UPDATE_AUTHOR:
			return {
				...state,
				authors: state.authors.map((author) => {
					if (author._id === action.payload._id) {
						author.name = action.payload.name;
						author.email = action.payload.email;
						author.image = action.payload.image;
					}
					return author;
				}),
			};

		default:
			return state;
	}
};
