import { combineReducers } from 'redux';
import articleReducer from './articleReducer';
import authorReducer from './authorReducer';
import categoryReducer from './categoryReducer';

const rootReducer = combineReducers({
	articles: articleReducer,
	authors: authorReducer,
	categories: categoryReducer,
});

export default rootReducer;
