import { categoryActionType } from '../actions/actionTypes/categoryActionType';

const initialState = {
	categories: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case categoryActionType.FETCH_CATEGORIES:
			return { ...state, categories: [...action.payload] };
		case categoryActionType.DELETE_CATEGORY:
			return { ...state, categories: state.categories.filter((category) => category._id !== action.payload) };
		case categoryActionType.ADD_CATEGORY:
			return { ...state, categories: [action.payload, ...state.categories] };
		case categoryActionType.UPDATE_CATEGORY:
			return {
				...state,
				categories: state.categories.map((category) => {
					if (category._id === action.payload._id) {
						category.name = action.payload.name;
					}
					return category;
				}),
			};
		default:
			return state;
	}
};
