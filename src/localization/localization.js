import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({
	en: {
		home: 'Home',
		article: 'Article',
		author: 'Author',
		category: 'Category',
		language: 'Language',
		search: 'Search',

		next: 'Next',
		previous: 'Previous',

		addArticle: 'Add Article',
		updateArticle: 'Update Article',

		title: 'Title',
		description: 'Description',
		selectAuthor: 'Select Author',

		submit: 'Submit',
		update: 'Update',

		authorName: 'Author Name',
		email: 'Email',

		No: '#',
		image: 'Image',
		action: 'Action',

		edit: 'Edit',
		delete: 'Delete',

		inputCategory: 'Input Category',
		categoryName: 'Category Name',
		save: 'Save',
	},
	kh: {
		home: 'ទំព័រដើម',
		article: 'អត្ថបទ',
		author: 'អ្នកនិពន្ធ',
		category: 'ប្រភេទអត្ថបទ',
		language: 'ភាសា',
		search: 'ស្វែងរក',

		next: 'ទៅមុខ',
		previous: 'បកក្រោយ',

		addArticle: 'បន្ថែមអត្ថបទ',
		updateArticle: 'កែប្រែអត្ថបទ',

		title: 'ចំណងជើង',
		description: 'ការពីពណ៍នា',
		selectAuthor: 'ជ្រើសរើសអ្នកនិពន្ធ',

		submit: 'បញ្ចូន',
		update: 'កែប្រែ',

		authorName: 'ឈ្មោះអ្នកនិពន្ធ',
		email: 'អ៊ីម៉ែល',

		No: 'លេខរៀង',
		image: 'រូបថត',
		action: 'សកម្មភាព',

		edit: 'កែ',
		delete: 'លុប',

		inputCategory: 'បញ្ចូលប្រភេទអត្ថបទ',
		categoryName: 'ឈ្មោះប្រភេទអត្ថបទ',
		save: 'រក្សា',
	},
});
